const getUserByUsername = require('../../utils/getUserByUsername')

async function headpat({ bot, message, context }) {
  const roomID = message.rid
  const user = await getUserByUsername(message.u.username)

  // Get the arguments
  let targetUser = context.argumentList[0]
  targetUser = await getUserByUsername(targetUser)

  // Send the message
  message = await bot.sendToRoom(
    `_Headpats ${targetUser.name}! :cat_headpat:_`,
    roomID,
  )
}

module.exports = {
  description: 'Give someone headpats!',
  help: `${process.env.ROCKETCHAT_PREFIX} headpat <user>`,
  call: headpat,
}
