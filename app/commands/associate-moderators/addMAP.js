const getUserByUsername = require('../../utils/getUserByUsername')
const getChannelsByChannelName = require('../../utils/getChannelsByChannelName')

const addUserToRole = require('../../utils/addUserToRole')
const userHasRole = require('../../utils/userHasRole')

const inviteUserToChannels = require('../../utils/inviteUserToChannels')

const defaultChannels = [
  'arts-and-crafts',
  'life-experiences',
  'miscellaneous',
  'serious',
  'support-requests',
  'venting',
  'food',
  'fun-and-games',
  'general',
  'media',
  'news',
  'proposals',
  'proposals-discussions',
  'proposals-polls',
  'social-media',
  'spam',
  'stem',
]

async function addMAP({ bot, message, context }) {
  const roomID = message.rid
  const user = await getUserByUsername(message.u.username)

  // Get the targetUser object
  let targetUser = context.argumentList[0]
  targetUser = await getUserByUsername(targetUser)

  // Check if the targetUser does not already have the MAP role
  if (!userHasRole(targetUser, 'MAP')) {
    // Add the MAP role to targetUser
    await bot.sendToRoom(
      'Giving ' + targetUser.name + ' the MAP role...',
      roomID,
    )
    await addUserToRole(targetUser, 'MAP')

    // Invite the targetUser to default channels
    await bot.sendToRoom(
      'Inviting ' + targetUser.name + ' to the default channels...',
      roomID,
    )
    let channels = await getChannelsByChannelName(defaultChannels)
    let invites = await inviteUserToChannels(targetUser, channels)
    invites.forEach((invite) => {
      console.log('THIS IS A INVITE OR AN ERROR FOR DEBUGGING:', invite)
    })
     // Initiate new direct message with MAPped member
     let message_to_send =
     '***Congrats on earning the MAP role! If you would like, you may send anonymous feedback to staff using the !map feedback command here in this DM room (requires E2E encryption in this DM room to be disabled).*** \n * '
   message_to_send = `${message_to_send} ${feedback_message} \n`
   await bot.sendDirectToUser(message_to_send, message.u.username)
    await bot.sendToRoom(
      'Finished inviting ' + targetUser.name + ' to the default channels! :D',
      roomID,
    )
  } else {
    // User already has the MAP role, no need to do anything else
    const response = targetUser.name + ' already has the MAP role'
    const sentMsg = await bot.sendToRoom(response, roomID)
    return
  }
}

module.exports = {
  description:
    'Give a member the MAP role and invite them to the appropriate channels.',
  help: `${process.env.ROCKETCHAT_PREFIX} addMAP <username>`,
  requireOneOfRoles: [
    'admin',
    'Tech Admin',
    'Community Admin',
    'Moderator',
    'Associate Moderator',
  ],
  call: addMAP,
}
