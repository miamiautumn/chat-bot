const getUserByUsername = require('../../utils/getUserByUsername')
const getChannelsByChannelName = require('../../utils/getChannelsByChannelName')

const addUserToRole = require('../../utils/addUserToRole')
const userHasRole = require('../../utils/userHasRole')

const inviteUserToChannels = require('../../utils/inviteUserToChannels')

const defaultChannels = [
  'food',
  'fun-and-games',
  'general',
  'media',
  'news',
  'proposals',
  'proposals-discussions',
  'proposals-polls',
  'social-media',
  'spam',
  'stem',
]

async function addNonMAP({ bot, message, context }) {
  const roomID = message.rid
  const user = await getUserByUsername(message.u.username)

  // Get the targetUser object
  let targetUser = context.argumentList[0]
  targetUser = await getUserByUsername(targetUser)

  // Check if the targetUser does not already have the Non-MAP role
  if (!userHasRole(targetUser, 'Non-MAP')) {
    // Add the MAP role to targetUser
    await bot.sendToRoom(
      'Giving ' + targetUser.name + ' the Non-MAP role...',
      roomID,
    )
    await addUserToRole(targetUser, 'Non-MAP')

    // Invite the targetUser to default channels
    let channels = await getChannelsByChannelName(defaultChannels)
    await bot.sendToRoom(
      'Inviting ' + targetUser.name + ' to the default Non-MAP channels...',
      roomID,
    )
    let invites = await inviteUserToChannels(targetUser, channels)
    await bot.sendToRoom(
      'Finished inviting ' +
        targetUser.name +
        ' to the default Non-MAP channels! :D',
      roomID,
    )
  } else {
    // User already has the MAP role, no need to do anything else
    const response = targetUser.name + ' already has the Non-MAP role'
    const sentMsg = await bot.sendToRoom(response, roomID)
    return
  }
}

module.exports = {
  description:
    'Give a non-map the Non-MAP role and invite them to the appropriate channels.',
  help: `${process.env.ROCKETCHAT_PREFIX} addNon-MAP <username>`,
  requireOneOfRoles: [
    'admin',
    'Tech Admin',
    'Community Admin',
    'Moderator',
    'Associate Moderator',
  ],
  call: addNonMAP,
}
