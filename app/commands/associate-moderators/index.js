const addMAP = require('./addMAP')
const userInfo = require('./userInfo')
const addNonMAP = require('./addNonMAP')

module.exports = {
  commands: {
    'Associate Moderation Commands': { addMAP, userInfo, addNonMAP },
  },
}
