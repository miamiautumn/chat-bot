const roomInfo = require('./roomInfo')
const testCommand = require('./testCommand')
const testCooldown = require('./testCooldown')
const addRole = require('./addRole')
const removeRole = require('./removeRole')

module.exports = {
  commands: {
    'Development Commands': {
      roomInfo,
      testCommand,
      testCooldown,
      addRole,
      removeRole,
    },
  },
}
