const getUserByUsername = require('../../utils/getUserByUsername')

const removeUserFromRoles = require('../../utils/removeUserFromRoles')
const userHasRole = require('../../utils/userHasRole')

async function removeRole({ bot, message, context }) {
  const roomID = message.rid
  const user = await getUserByUsername(message.u.username)

  // Get the arguments object
  let targetRole = context.argumentList[0]
  let targetUser = context.argumentList[1]
  targetUser = await getUserByUsername(targetUser)

  // Check if the targetUser does not already have the role
  if (userHasRole(targetUser, targetRole)) {
    // Add the role to targetUser
    await bot.sendToRoom(
      `Removing the ${targetRole} role from ${targetUser.name}...`,
      roomID,
    )
    await removeUserFromRoles(targetUser, [targetRole])
  } else {
    // User already has the role, no need to do anything else
    const response = targetUser.name + ' does not have the role'
    const sentMsg = await bot.sendToRoom(response, roomID)
    return
  }
}

module.exports = {
  description: 'Remove a role from a member.',
  help: `${process.env.ROCKETCHAT_PREFIX} removeRole <role> <username>`,
  requireOneOfRoles: ['admin', 'Tech Admin'],
  call: removeRole,
}
