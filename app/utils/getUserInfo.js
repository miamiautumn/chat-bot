const { api } = require('@rocket.chat/sdk')

const getUserInfo = async (user) => {
  let result = await api.get('users.info', {
    userId: user._id,
    fields: { userRooms: 1 },
  })
  if (result.success) {
    return result.user
  } else {
    console.error(result)
  }
}

module.exports = getUserInfo
