const { api } = require('@rocket.chat/sdk')

const inviteUserToChannel = async (user, channel) => {
  try {
    let result = await api.post('channels.invite', {
      roomId: channel._id,
      userId: user._id,
    })
    if (result.success) {
      return result.channel
    } else {
      console.error(result)
    }
  } catch (error) {
    console.error(result)
    return error
  }
}

module.exports = inviteUserToChannel
